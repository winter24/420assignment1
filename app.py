from flask import Flask, request, render_template
import random


app = Flask(__name__)

@app.route("/")
@app.route("/home")
def home():
    return render_template('exercises.html')

@app.route("/exercise1")
def exercise1():
    return render_template('exercise1.html')


@app.route("/exercise2")
def exercise2():
    return render_template('exercise2.html')

@app.route("/exercise3")
def exercise3():
    return render_template('exercise3.html')

@app.route("/exercise4")
def exercise4():
    return render_template('exercise4.html')

@app.route("/exercise5")
def exercise5():
    return render_template('exercise5.html')

@app.route('/exercise1', methods=['POST'])
def exercise1_post():
    dividend = int(request.form['dividend'])
    divisor = int(request.form['divisor'])

    # Determine if the dividend is odd or even or a multiple of 4
    dividend_type = "odd" if dividend % 2 != 0 else "even"
    if dividend % 4 == 0:
        dividend_type = "multiple of 4"

    # Determine if the dividend divides evenly by the divisor
    divides_evenly = dividend % divisor == 0

    return render_template('exercise1_result.html', dividend=dividend, divisor=divisor, dividend_type=dividend_type, divides_evenly=divides_evenly)

@app.route('/exercise2', methods=['POST'])
def exercise2_post():
    name = request.form['name']
    grade = int(request.form['grade'])
    message =''
    if grade not in range(0,101):
        message = f"Invalid score {grade}. Please enter a number between 0 and 100."
        return render_template('exercise2_result.html', message = message, name = name)
    else:
        grades = {
            'A': 95,
            'B': 80,
            'C': 70,
            'D': 60,
            'F': 50,
            'F': 0
        }
        result = ""
        for c in grades:
            if grade >= grades[c]:
                result = c
                message = f'your score is {grade} so you get an {result}'
                break
                

    return render_template('exercise2_result.html', message = message, name = name)

@app.route('/exercise3', methods=['POST'])
def exercise3_post():
    value = float(request.form['value'])
    if value < 0:
            raise ValueError("Negative value")
    result = value ** 0.5
    return render_template('exercise3_result.html', result=result)



@app.route('/exercise4', methods=['POST'])
def exercise4_post():
    number = int(request.form['number'])
    random_number = 0
    counter =0
    while random_number != number:
        random_number = random.randint(0,100)
        counter += 1
    return render_template('exercise4_result.html', counter = counter, number = number)

@app.route('/exercise5', methods=['POST'])
def exercise5_post():
    number = int(request.form['number'])
    sequence =''
    if number <=0:
        sequence = f'{number}: error... limit is >=0'
        return sequence
    fib_sequence = [0,1]
    while fib_sequence[-1] < number:
        next_number = fib_sequence[-1] + fib_sequence[-2]
        fib_sequence.append(next_number)
    num_list = fib_sequence[:-1]
    string_list = [str(element) for element in num_list[:number]]
    result_string = ", ".join(string_list)
    sequence = f'{result_string}'
    return render_template('exercise5_result.html', sequence= sequence, number = number)


